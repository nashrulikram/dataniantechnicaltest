Description:
The program allows users to manage multiple devices, sensors, and stations through a console-based interface.

Features:
Add Device: Users can add new devices by providing an ID and a name.
Add Sensor: Users can add new sensors by providing an ID and a type.
Add Station: Users can add new stations by providing an ID and a location.
Display Devices: Users can view a list of all added devices along with their IDs and names.
Display Sensors: Users can view a list of all added sensors along with their IDs and types.
Display Stations: Users can view a list of all added stations along with their IDs and locations.
Exit: Users can exit the program.

Notes:
The program is a simple demonstration of basic CRUD operations and user interaction in a console environment.
It can be extended with additional features like update and delete functionalities, data validation, error handling, and persistence.