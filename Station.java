public class Station {
    private int id;
    private String location;
    
    public Station(int id, String location) {
        this.id = id;
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public String getLocation() {
        return location;
    }
}
