import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    private static ArrayList<Device> devices = new ArrayList<>();
    private static ArrayList<Sensor> sensors = new ArrayList<>();
    private static ArrayList<Station> stations = new ArrayList<>();
    private static Scanner scanner = new Scanner(System.in);
    
    public static void main(String[] args) {
        // Sample data
        devices.add(new Device(1, "Device 1"));
        sensors.add(new Sensor(1, "Temperature"));
        stations.add(new Station(1, "Station 1"));

        // Menu options
        while (true) {
            System.out.println("1. Add Device");
            System.out.println("2. Add Sensor");
            System.out.println("3. Add Station");
            System.out.println("4. Display Devices");
            System.out.println("5. Display Sensors");
            System.out.println("6. Display Stations");
            System.out.println("0. Exit");
            System.out.print("Enter your choice: ");
            
            int choice = scanner.nextInt();
            scanner.nextLine(); // Consume newline
            
            switch (choice) {
                case 1:
                    addDevice();
                    break;
                case 2:
                    addSensor();
                    break;
                case 3:
                    addStation();
                    break;
                case 4:
                    displayDevices();
                    break;
                case 5:
                    displaySensors();
                    break;
                case 6:
                    displayStations();
                    break;
                case 0:
                    System.out.println("Exiting...");
                    System.exit(0);
                default:
                    System.out.println("Invalid choice! Please try again.");
            }
        }
    }
    
    // Method to add a device
    private static void addDevice() {
        System.out.print("Enter device ID: ");
        int id = scanner.nextInt();
        scanner.nextLine(); // Consume newline
        System.out.print("Enter device name: ");
        String name = scanner.nextLine();
        devices.add(new Device(id, name));
        System.out.println("Device added successfully.");
    }
    
    // Method to add a sensor
    private static void addSensor() {
        System.out.print("Enter sensor ID: ");
        int id = scanner.nextInt();
        scanner.nextLine(); // Consume newline
        System.out.print("Enter sensor type: ");
        String type = scanner.nextLine();
        sensors.add(new Sensor(id, type));
        System.out.println("Sensor added successfully.");
    }
    
    // Method to add a station
    private static void addStation() {
        System.out.print("Enter station ID: ");
        int id = scanner.nextInt();
        scanner.nextLine(); // Consume newline
        System.out.print("Enter station location: ");
        String location = scanner.nextLine();
        stations.add(new Station(id, location));
        System.out.println("Station added successfully.");
    }
    
    // Method to display devices
    private static void displayDevices() {
        System.out.println("Devices:");
        for (Device device : devices) {
            System.out.println("ID: " + device.getId() + ", Name: " + device.getName());
        }
    }
    
    // Method to display sensors
    private static void displaySensors() {
        System.out.println("Sensors:");
        for (Sensor sensor : sensors) {
            System.out.println("ID: " + sensor.getId() + ", Type: " + sensor.getType());
        }
    }
    
    // Method to display stations
    private static void displayStations() {
        System.out.println("Stations:");
        for (Station station : stations) {
            System.out.println("ID: " + station.getId() + ", Location: " + station.getLocation());
        }
    }
}
